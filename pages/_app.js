function MyApp({ Component, pageProps }) {
  console.log('App Render');
  return <Component 
    theme='default'
    {...pageProps} 
  />
}

export default MyApp
